################
fuse-blockshield
################


`fuse-blockshield` provides FUSE filesystems with a block-level mapping of
a virtual file onto its backing file.

Typically, in the backing file, each virtual block is augmented with metadata
providing integrity checking or more.


Usage
#####

See each file's documentation.


fuse_filelink.c
***************

This is a simple file proxy filesystem.
Using this is equivalent as performing a bind-mount of a source to a
destination: the virtual file has an identity mapping to the backing
file.

The source code also serves as an entry point to read more complex C
FUSE filesystems.


fuse_blockshield.c
******************

This filesystem is used to "shield" blocks in a virtual file, adding
additional "shielding" metadata.

The backing file can be read or written to, but can't grow.


Shielding Options
=================

The shielding options are:

- Nothing, ie. the file system performs an identity mapping
  of the backing file to the virtual file.

  This is useless and left as a way to show the structure of the
  source code.

- Integrity checking: when a block is accessed, if there has been a
  data corruption, the hash in metadata *should* detect the corruption
  and allow reporting of an I/O error instead of returning garbage.

  This is not totally useless, I swear there are use cases for this,
  such as performing automatic verification that entries were written
  fully.

- Error detection and correction via a forward error correction
  metadata.

  This could be helpful in case you want to use this filesystem to
  protect data that would reside on a medium which could get
  erasure-type corruption.

  Note that in case the data is WORM (write-once, read many),
  you're much better off using FEC schemes which perform interleaved
  FEC encoding, such as parchives, or dm-verity FEC.


Compile-Time Options
====================

Compile-time options are providing either:

- No shielding

- Integrity checking, such as a SHA256 hash;

- Reed-Solomon (RS) FEC data providing error correction.

The implementation can provide SHA256 support using the libcrypto API
(from OpenSSL or compatible) ; when using OpenSSL, the resulting
executable uses the GPL-3.0-only or Proprietary license, as the product
includes software developed by the OpenSSL Project for use in the
`OpenSSL Toolkit <http://www.openssl.org/>`_.
Note that OpenSSL is `Apache-2.0
<https://spdx.org/licenses/Apache-2.0.html>`_
licensed.

The implementation can provide hash support using the HACL\* API
(the library supports MD5, SHA1, SHA2, or SHA3 algorithms),
with SHA256 and SHA3-256 implemented.
THE HACL\* code is licensed under the MIT license; when using HACL\*
this code uses the GPL-3.0-only or Proprietary license.
Please refer to the `HACL\* project`_ documentation for more
information on HACL\*.

.. _HACL\* project: https://github.com/hacl-star/hacl-star

The implementation provides RS support using the rslib API,
from (https://gitlab.com/exmakhina/rslib or compatible) ; when using
this, the resulting executable uses the license GPL-2.0-only.

