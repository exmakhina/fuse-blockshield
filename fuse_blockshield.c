// SPDX-License-Identifier: GPL-3.0-only OR Proprietary
// Note: license possibilities depend on used compile-time options
// SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-fuse_blockshield@zougloub.eu>
/*! \file
  \brief FUSE filesystem mapping one backing file to a virtual file;
  the virtual file blocks are mapped to backing file blocks that
  contain the original data plus FEC data.
  The FEC data can be either concatenated with the data block,
  or stored separately.
 */
#define FUSE_USE_VERSION 31

#include <fuse3/fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>

#if defined(BLOCKSHIELD_USE_FEC_RS)
# include "rslib.h"
# define FEC_SIZE 16
#elif defined(BLOCKSHIELD_USE_SHA256)
# include <openssl/sha.h>
# define FEC_SIZE SHA256_DIGEST_LENGTH
#elif defined(BLOCKSHIELD_USE_SHA2_256_HACL)
# include <_hacl/Hacl_Hash_SHA2.h>
# define FEC_SIZE 32
#elif defined(BLOCKSHIELD_USE_SHA3_256_HACL)
# include <_hacl/Hacl_Hash_SHA3.h>
# define FEC_SIZE 32
#elif defined(BLOCKSHIELD_USE_CRC64)
/* ECMA-182 CRC64
   cf. https://www.ecma-international.org/wp-content/uploads/ECMA-182_1st_edition_december_1992.pdf
   Annex B "CRC generation"
*/
# define CRC_POLY_64 0x42F0E1EBA9EA3693ULL
# define CRC_INIT_64 0
# define CRC_FINI_64 0
static uint8_t crc64_tab[256];
# define FEC_SIZE 8
#else
# define FEC_SIZE 0
#endif

//#define DEBUG_BLOCKSHIELD

#if defined(DEBUG_BLOCKSHIELD)
# define debug printf
#else
# define debug(...)
#endif

#if !defined(VIRTUAL_BLOCK_SIZE)
# define VIRTUAL_BLOCK_SIZE 4096
#endif

#define BACKING_BLOCK_SIZE (VIRTUAL_BLOCK_SIZE+FEC_SIZE)
//#define SEPARATED_FEC // FEC information is located in a separate area

// http://www.pixelbeat.org/programming/gcc/static_assert.html
#define ASSERT_CONCAT_(a, b) a##b
#define ASSERT_CONCAT(a, b) ASSERT_CONCAT_(a, b)
/* These can't be used after statements in c89. */
#ifdef __COUNTER__
  #define STATIC_ASSERT(e,m) \
    ;enum { ASSERT_CONCAT(static_assert_, __COUNTER__) = 1/(int)(!!(e)) }
#else
  /* This can't be used twice on the same line so ensure if using in headers
   * that the headers are not included twice (by wrapping in #ifndef...#endif)
   * Note it doesn't cause an issue when used on same line of separate modules
   * compiled with gcc -combine -fwhole-program.  */
  #define STATIC_ASSERT(e,m) \
    ;enum { ASSERT_CONCAT(assert_line_, __LINE__) = 1/(int)(!!(e)) }
#endif


STATIC_ASSERT(sizeof(off_t) == 8, "off_t is wong");

#define min(x, y) ((x) < (y) ? x : y)

#if defined(BLOCKSHIELD_USE_SHA256)
void compute_sha256(const uint8_t * data, size_t data_len, uint8_t * hash)
{
    SHA256_CTX sha256_ctx;
    SHA256_Init(&sha256_ctx);
    SHA256_Update(&sha256_ctx, data, data_len);
    SHA256_Final(hash, &sha256_ctx);
}
#elif defined(BLOCKSHIELD_USE_SHA2_256_HACL)
void compute_sha2_256(const uint8_t * data, size_t data_len, uint8_t * digest)
{
	Hacl_Streaming_MD_state_32 * hash_state = Hacl_Streaming_SHA2_create_in_256();
	// TODO shared object + Hacl_Streaming_SHA2_init_256

	uint8_t const * buf = data;
	size_t len = data_len;
	while (len > UINT32_MAX) {
		Hacl_Streaming_SHA2_update_256(hash_state, buf, UINT32_MAX);
		len -= UINT32_MAX;
		buf += UINT32_MAX;
	}
	Hacl_Streaming_SHA2_update_256(hash_state, buf, (uint32_t) len);
	Hacl_Streaming_SHA2_finish_256(hash_state, digest);
	Hacl_Streaming_SHA2_free_256(hash_state);
}
#elif defined(BLOCKSHIELD_USE_SHA3_256_HACL)
void compute_sha3_256(const uint8_t * data, size_t data_len, uint8_t * digest)
{
	Hacl_Streaming_Keccak_state *hash_state;
	hash_state = Hacl_Streaming_Keccak_malloc(Spec_Hash_Definitions_SHA3_256);
	// TODO shared object + Hacl_Streaming_Keccak_reset
	uint8_t const * buf = data;
	size_t len = data_len;
	while (len > UINT32_MAX) {
		Hacl_Streaming_Keccak_update(hash_state, buf, UINT32_MAX);
		len -= UINT32_MAX;
		buf += UINT32_MAX;
	}
	Hacl_Streaming_Keccak_update(hash_state, buf, (uint32_t) len);
	Hacl_Streaming_Keccak_finish(hash_state, digest);
	Hacl_Streaming_Keccak_free(hash_state);
}
#elif defined(BLOCKSHIELD_USE_CRC64)
void compute_crc64(const uint8_t * data, size_t data_len, uint8_t* digest)
{
	uint64_t crc = CRC_INIT_64;
	uint64_t const mask = 0xff;
	for (size_t i = 0; i < data_len; i++) {
		crc = (crc << 8) ^ crc64_tab[((crc >> 56) ^ (uint64_t) *data++) & mask ];
	}
	return crc ^ CRC_FINI_64;
}
#endif

struct Filesystem {
	const char *source_path;
	void * codec;
	int f;
	size_t bs;
	size_t eh;
	off_t size;
};

uint16_t Fletcher16(uint8_t const * data, int count )
{
   uint16_t sum1 = 0;
   uint16_t sum2 = 0;
   int index;

   for ( index = 0; index < count; ++index )
   {
      sum1 = (sum1 + data[index]) & 0xFF;
      sum2 = (sum2 + sum1) & 0xFF;
   }

   return (sum2 << 8) | sum1;
}

static void encode(void * codec, const uint8_t *input, uint8_t *output)
{
	memcpy(output, input, VIRTUAL_BLOCK_SIZE);

	debug("Encoding data h=%04x\n", Fletcher16(input, VIRTUAL_BLOCK_SIZE));

#if defined(BLOCKSHIELD_USE_FEC_RS)
	(void) codec;
	struct rs_control * rs = codec;
	uint16_t par[FEC_SIZE*sizeof(uint16_t)];
	memset(par, 0, sizeof(par));

	encode_rs16(rs, (uint16_t*)output, VIRTUAL_BLOCK_SIZE/sizeof(uint16_t), &par[0], 0);

	for (int i = 0; i < rs->codec->nroots; i++) {
		((uint16_t*)&output[VIRTUAL_BLOCK_SIZE])[i] = par[i];
	}
#elif defined(BLOCKSHIELD_USE_SHA256)
	(void) codec;
	compute_sha256(output, VIRTUAL_BLOCK_SIZE, &output[VIRTUAL_BLOCK_SIZE]);
#elif defined(BLOCKSHIELD_USE_SHA2_256_HACL)
	(void) codec;
	compute_sha2_256(output, VIRTUAL_BLOCK_SIZE, &output[VIRTUAL_BLOCK_SIZE]);
#elif defined(BLOCKSHIELD_USE_SHA3_256_HACL)
	(void) codec;
	compute_sha3_256(output, VIRTUAL_BLOCK_SIZE, &output[VIRTUAL_BLOCK_SIZE]);
#elif defined(BLOCKSHIELD_USE_CRC64)
	(void) codec;
	compute_crc64(output, VIRTUAL_BLOCK_SIZE, &output[VIRTUAL_BLOCK_SIZE]);
#else
	(void) codec;
#endif

	debug("Encoded data hdat=%04x hpar=%04x\n", Fletcher16(output, VIRTUAL_BLOCK_SIZE), Fletcher16(&output[VIRTUAL_BLOCK_SIZE], 16));

}

static int decode(void * codec, const uint8_t *input, uint8_t *output)
{
	int res;
#if defined(BLOCKSHIELD_USE_FEC_RS)
	struct rs_control * rs = codec;
	uint16_t par[FEC_SIZE*sizeof(uint16_t)];
	for (int i = 0; i < rs->codec->nroots; i++) {
		par[i] = ((uint16_t*)&input[VIRTUAL_BLOCK_SIZE])[i];
	}
	memcpy(output, input, VIRTUAL_BLOCK_SIZE);
	int neras = 0;
	void * derrlocs = NULL;
	res = decode_rs16(rs, (uint16_t*)output, &par[0], VIRTUAL_BLOCK_SIZE/sizeof(uint16_t), NULL, neras, derrlocs, 0, NULL);
	if (res > 0) {
		debug("Corrected %d\n", res);
		res = 0;
	}
	else if (res < 0) {
		debug("Decoding error %d ?= %d\n", res, -EBADMSG);
	}
	else {
		debug("No error detected\n");
	}
#elif defined(BLOCKSHIELD_USE_SHA256)
	(void) codec;
	uint8_t hash[FEC_SIZE];
	compute_sha256(input, VIRTUAL_BLOCK_SIZE, hash);
	if (memcmp(&input[VIRTUAL_BLOCK_SIZE], hash, FEC_SIZE) != 0) {
		debug("Hash mismatch: computed ");
		for (int i = 0; i < FEC_SIZE; i++) {
			debug("%02x", hash[i]);
		}
		debug(" provided ");
		for (int i = 0; i < FEC_SIZE; i++) {
			debug("%02x", input[VIRTUAL_BLOCK_SIZE + i]);
		}
		debug("\n");
		return -EBADMSG;
	}
	res = 0;
	memcpy(output, input, VIRTUAL_BLOCK_SIZE);
#else
	memcpy(output, input, VIRTUAL_BLOCK_SIZE);
	(void) codec;
	res = 0;
#endif
	return res;
}

static void* fs_init(struct fuse_conn_info */*conn*/, struct fuse_config *cfg)
{
	cfg->kernel_cache = 1;
	return fuse_get_context()->private_data;
}

static int fs_getattr(const char */*path*/, struct stat *stbuf, struct fuse_file_info */*fi*/)
{
	debug("Getattr\n");
	struct Filesystem *fs = (struct Filesystem *)fuse_get_context()->private_data;
	int res = lstat(fs->source_path, stbuf);
	if (res == -1) {
		return -errno;
	}

	// File size is self.bs bytes for every self.eh bytes in the source file
	stbuf->st_size = fs->size;
	stbuf->st_blocks = fs->size / VIRTUAL_BLOCK_SIZE;
	stbuf->st_blksize = VIRTUAL_BLOCK_SIZE;
	stbuf->st_mode = 0100664; // Set st_mode to regular file mode

	return 0;
}

static int fs_open(const char */*path*/, struct fuse_file_info */*fi*/)
{
	debug("Open\n");
	return 0;
}

static int fs_read_block(off_t block, uint8_t * buf)
{
	struct Filesystem *fs = (struct Filesystem *)fuse_get_context()->private_data;

	debug("Read block %jd @ 0x%jx\n", block, block * fs->eh);

	uint8_t block_data[BACKING_BLOCK_SIZE];

#if !defined(SEPARATED_FEC)
	size_t bytes_read = pread(fs->f, block_data, fs->eh, block * fs->eh);
	if (bytes_read != fs->eh) {
		return -EIO;
	}
#else
	size_t bytes_read = pread(fs->f, block_data, fs->bs, block * fs->bs);
	if (bytes_read != fs->bs) {
		return -EIO;
	}
	bytes_read += pread(fs->f, &block_data[VIRTUAL_BLOCK_SIZE], FEC_SIZE, fs->size + block * FEC_SIZE);
	if (bytes_read != fs->eh) {
		return -EIO;
	}
#endif

#if defined(DEBUG_BLOCKSHIELD)
	for (int row = 0; row < 64; row++) {
		for (int col = 0; col < 64; col++) {
			uint8_t v = block_data[row*64+col];
			if (isprint(v)) {
				printf(" %c ", v);
			}
			else {
				printf(" %02x", block_data[row*64+col]);
			}
		}
		printf("\n");
	}
	for (int row = 64; row < 65; row++) {
		for (int col = 0; col < FEC_SIZE; col++) {
			uint8_t v = block_data[row*64+col];
			if (isprint(v)) {
				printf(" %c ", v);
			}
			else {
				printf(" %02x", block_data[row*64+col]);
			}
		}
		printf("\n");
	}
#endif // defined(DEBUG_BLOCKSHIELD)

	int rs_res = decode(fs->codec, &block_data[0], buf);
	if (rs_res != 0) {
		return -EIO;
	}

	return 0;
}

static int fs_write_block(off_t block, uint8_t const * buf)
{
	struct Filesystem *fs = (struct Filesystem *)fuse_get_context()->private_data;

	debug("Write block %jd\n", block);

	uint8_t encoded_block_data[fs->eh];
	encode(fs->codec, buf, encoded_block_data);

#if !defined(SEPARATED_FEC)
	size_t bytes_written = pwrite(fs->f, encoded_block_data, fs->eh, block * fs->eh);
	if (bytes_written != fs->eh) {
		return -EIO;
	}
#else
	// Write data
	size_t bytes_written = pwrite(fs->f, encoded_block_data, VIRTUAL_BLOCK_SIZE, block * VIRTUAL_BLOCK_SIZE);
	if (bytes_written != VIRTUAL_BLOCK_SIZE) {
		return -EIO;
	}
	// Write FEC
	bytes_written += pwrite(fs->f, &encoded_block_data[VIRTUAL_BLOCK_SIZE], FEC_SIZE, fs->size + block * FEC_SIZE);
	if (bytes_written != BACKING_BLOCK_SIZE) {
		return -EIO;
	}
#endif

	return 0;
}


static int fs_read(const char */*path*/, char *buf, size_t size, off_t offset, struct fuse_file_info */*fi*/)
{
	// Implement the read function here
	struct Filesystem *fs = (struct Filesystem *)fuse_get_context()->private_data;
	off_t file_size = fs->size;

	if (offset >= file_size) {
		debug("Zigouilling read\n");
		return 0;
	}
	else if (offset + (off_t)size > file_size) {
		debug("Truncating read, end %jd > %jd\n", offset + size, file_size);
		size = size - ((offset + size) - file_size);
		debug("Truncated read to %jd %jd == %jd\n", size, offset + size, fs->size);
	}

	if (size == 0) {
		return 0;
	}

	debug("Reading @%jd, %zu\n", offset, size);

	size_t block_size = fs->bs;

	// Calculate the first and last blocks
	off_t beg_block = offset / fs->bs;

#if defined(DEBUG_BLOCKSHIELD)
	off_t end_block = (offset + size + block_size - 1) / block_size;
	debug("R Blocks [%jd,%jd[\n", beg_block, end_block);
#endif

	// Calculate the position within the first and last block
	size_t beg_block_pos = offset % block_size;
	//size_t end_block_pos = (offset + size) % block_size;

	size_t read_size = 0;

	off_t block = beg_block;

	if (beg_block_pos != 0) {
		uint8_t data[VIRTUAL_BLOCK_SIZE];
		int chunk = min(block_size-beg_block_pos, size);
		int res = fs_read_block(block++, data);
		if (res != 0) {
			goto err;
		}
		memcpy(buf, &data[beg_block_pos], chunk);
		read_size += chunk;
	}

	while (read_size + block_size <= size) {
		int res = fs_read_block(block++, (uint8_t*)&buf[read_size]);
		if (res != 0) {
			goto err;
		}
		read_size += block_size;
	}

	if (read_size != size) {
		debug("Read trailing block from %zu\n", read_size);
		uint8_t data[VIRTUAL_BLOCK_SIZE];
		int res = fs_read_block(block, data);
		if (res != 0) {
			goto err;
		}
		memcpy((uint8_t*)&buf[read_size], data, size-read_size);
		read_size += size-read_size;
	}

	if (read_size != 0) {
		debug("Finished reading %jd\n", read_size);
		return read_size;
	}

 err:
	debug("Read nothing, returning EIO\n");
	return -EIO;
}

static int fs_write(const char */*path*/, const char *buf, size_t size, off_t offset, struct fuse_file_info */*fi*/)
{
	// Implement the write function here
	struct Filesystem *fs = (struct Filesystem *)fuse_get_context()->private_data;
	off_t const file_size = fs->size;
	size_t const block_size = fs->bs;
	int res;

	debug("Writing @%jd, %zu\n", offset, size);

	if (offset >= file_size) {
		debug("Zigouilling write buffer\n");
		return 0;
	}
	else if (offset + (off_t)size > file_size) {
		debug("Want to go over %jd\n", offset + size - file_size);
		size = size - ((offset + size) - file_size);
		debug("Truncating write buffer to %zd\n", size);
	}

	if (size == 0) {
		return 0;
	}

	// Calculate the first and last blocks
	off_t beg_block = offset / fs->bs;
	off_t end_block = (offset + size) / fs->bs;
	off_t block = beg_block;

	debug("Blocks [%jd,%jd[\n", beg_block, end_block);

	size_t beg_block_pos = offset % fs->bs;

	size_t written_size = 0;
	int first_block_write = min(size, fs->bs - beg_block_pos);

	if (beg_block_pos != 0) {
		debug("Write in the middle of a block\n");
		uint8_t data[block_size];
		res = fs_read_block(block, data);
		if (res < 0) {
			return 0;
		}
		memcpy(&data[beg_block_pos], buf, first_block_write);
		res = fs_write_block(block, data);
		if (res == 0) {
			written_size = first_block_write;
		}
		else {
			return 0;
		}
		block++;
	}

	if (written_size == size) {
		return written_size;
	}

	while (size - written_size >= fs->bs) {
		debug("Write full block\n");
		int a = (block-beg_block) * block_size - beg_block_pos;
		res = fs_write_block(block++, (uint8_t const*)&buf[a]);
		if (res == 0) {
			written_size += block_size;
		}
		else {
			return written_size;
		}
	}

	if (written_size == size) {
		return written_size;
	}

	{
		debug("Write end partial block %jd ..%jd\n", block, size-written_size);
		int a = (end_block - beg_block) * block_size - beg_block_pos;
		uint8_t data[block_size];
		res = fs_read_block(block, data);
		if (res < 0) {
			return 0;
		}
		memcpy(data, &buf[a], size - written_size);
		res = fs_write_block(block, data);
		if (res == 0) {
			written_size += size - written_size;
		}
		else {
			return written_size;
		}
	}

	return written_size;
}

static int fs_release(const char */*path*/, struct fuse_file_info */*fi*/)
{
	debug("Release\n");
	return 0;
}

static struct fuse_operations fs_ops = {
	.init = fs_init,
	.getattr = fs_getattr,
	.open = fs_open,
	.read = fs_read,
	.write = fs_write,
	.release = fs_release,
};

int main(int argc, char *argv[]) {
	int res;

	if (argc < 3) {
		fprintf(stderr, "Usage: %s <backing_file> <mount_point> [fuse options]\n", argv[0]);
		return 1;
	}

	struct Filesystem fs;
	fs.source_path = argv[1];

#if defined(BLOCKSHIELD_USE_FEC_RS)
	{
		int symsize = 16;
		int gfpoly = 0x1002d;
		int fcr = 0;
		int prim = 1;
		int nroots = FEC_SIZE / sizeof(uint16_t); /// ??
		fs.codec = init_rs(symsize, gfpoly, fcr, prim, nroots);
	}
#elif defined(BLOCKSHIELD_USE_CRC64)
	{
		for (uint64_t i = 0; i < 256; i++) {
			uint64_t crc = 0;
			uint64_t c = i << 56;
			for (uint64_t j = 0; j < 8; j++) {
				if ((crc ^ c) & ((uint64_t)(1)<<63)) {
					crc = (crc << 1) ^ CRC_POLY_64;
				}
				else {
					crc <<= 1;
				}
				c = c << 1;
			}
			crc64_tab[i] = crc;
		}
		fs.codec = NULL;
	}
#else
	fs.codec = NULL;
#endif

	fs.f = open(fs.source_path, O_RDWR | O_SYNC);
	fs.bs = VIRTUAL_BLOCK_SIZE;
	fs.eh = BACKING_BLOCK_SIZE;
	fs.size = (lseek(fs.f, 0, SEEK_END) / fs.eh) * fs.bs;

	struct fuse_args args = FUSE_ARGS_INIT(argc-2, &argv[2]);
	fuse_opt_parse(&args, NULL, NULL, NULL);

	struct fuse *fuse = fuse_new(&args, &fs_ops, sizeof(fs_ops), &fs);
	if (!fuse) {
		perror("fuse_new");
		return 1;
	}

	res = fuse_mount(fuse, argv[2]);
	if (res != 0) {
		perror("fuse_mount");
		return 1;
	}

	struct fuse_session * session = fuse_get_session(fuse);
	res = fuse_set_signal_handlers(session);

	int ret = fuse_loop(fuse);

	fuse_unmount(fuse);
	fuse_destroy(fuse);
	fuse_opt_free_args(&args);

#if defined(BLOCKSHIELD_USE_FEC_RS)
	free_rs(fs.codec);
#endif

	close(fs.f);

	return ret;
}
