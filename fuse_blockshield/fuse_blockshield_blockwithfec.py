#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-only OR Proprietary
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-fuse_blockshield@zougloub.eu>

"""

This FUSE filesystem performs mapping a backing file to a virtual file;
each block in the virtual file corresponds to a block in the backing file,
the backing block being larger because it contains additional (FEC) data.

The virtual file block size of 4096, and the amount of FEC leading
to a silly backing block size, are deliberately chosen for a specific
use case where the MVCC is used over this filesystem.

"""


import io
import os
import sys
import stat
import errno
import logging
import hashlib
import array
import time

import llfuse
import reedsolo


class LazyBlock:
	def __init__(self, idx, data):
		self._idx = idx
		self._data = data

	def __str__(self):
		return "(block {} size {} hash {})".format(
		 self._idx,
		 len(self._data),
		 hashlib.md5(self._data).hexdigest(),
		)


logger = logging.getLogger()


class Filesystem(llfuse.Operations):
	def __init__(self, source_path):
		self.source_path = source_path
		self.rs = reedsolo.RSCodec(8, c_exp=16)
		self.f = os.open(self.source_path, os.O_RDWR | os.O_SYNC)

		self.bs = 4096
		z = self.encode(b"\0" * self.bs)
		self.eh = len(z)
		assert len(self.decode(z)) == self.bs

		with io.open(source_path, "rb") as fi:
			fi.seek(0, os.SEEK_END)
			size = fi.tell()

		# Get user size (floor rounded)
		self.size = size // self.eh * self.bs

		logger.info("Available front size: %d (%d blocks of %d)",
		 self.size, self.size // self.bs, self.bs)
		logger.info("Available back size:  %d (%d blocks of %d) padded to %d",
		 size // self.eh * self.eh, size // self.eh, self.eh, size)

	def encode(self, data):
		ret = array.array("H", self.rs.encode(array.array("H", data))).tobytes()
		return ret

	def decode(self, data):
		ret = array.array("H", self.rs.decode(array.array("H", data))[0]).tobytes()
		return ret

	def write_block(self, block, data):
		assert len(data) == self.bs, f"Want to write {len(data)}"
		f = self.f
		z = self.encode(data)
		assert len(z) == self.eh, f"Encoded to wrong length {len(z)}"
		logger.debug("put %s", LazyBlock(block, z))
		res = os.pwrite(f, z, block*self.eh)
		assert res == self.eh

	def read_block(self, block):
		f = self.f
		z = os.pread(f, self.eh, block * self.eh)
		assert z
		logger.debug("got %s", LazyBlock(block, z))
		assert len(z) == self.eh
		ret = self.decode(z)
		assert len(ret) == self.bs
		return ret

	def getattr(self, inode, ctx=None):
		logger.debug("Getattr %d", inode)
		entry = llfuse.EntryAttributes()

		st = os.stat(self.source_path)

		for attr in ('st_ino', 'st_mode', 'st_nlink', 'st_uid', 'st_gid',
		 'st_rdev', 'st_size', 'st_atime_ns', 'st_mtime_ns',
		'st_ctime_ns'):
			setattr(entry, attr, getattr(st, attr))
			logger.debug("Entry %s %s", attr, getattr(st, attr))

		if 1:
			entry.st_mode = 33204

		# File size is self.bs bytes for every self.eh bytes in the source file
		entry.st_size = self.size
		entry.st_ino = inode


		entry.generation = 0
		entry.entry_timeout = 5
		entry.attr_timeout = 5
		entry.st_blksize = self.bs
		entry.st_blocks = ((entry.st_size+entry.st_blksize-1) // entry.st_blksize)

		return entry

	def open(self, inode, flags, ctx):
		logger.debug("Open %d flags=%d", inode, flags)
		return inode

	def release(self, fh):
		logger.debug("Release %d", fh)
		return

	def write(self, fd, offset, buf):
		logger.debug("Writing @%d, %d", offset, len(buf))
		f = self.f

		if offset >= self.size:
			buf = b""
			logger.debug("Zigouilling write buffer")
		elif offset + len(buf) > self.size:
			logger.debug("Want to go over %d", offset + len(buf) - self.size)
			buf = buf[:-(offset + len(buf))- self.size]
			logger.debug("Truncating write buffer to %d", len(buf))

		if not buf:
			return 0

		beg_block = offset // self.bs
		end_block = (offset + len(buf)) // self.bs

		logger.debug("Blocks [%d,%d[", beg_block, end_block)

		# Calculate the position within the first and last block
		beg_block_pos = offset % self.bs
		end_block_pos = (offset + len(buf)) % self.bs
		if end_block_pos == 0:
			end_block_pos = None

		beg_aligned = beg_block_pos == 0
		end_aligned = end_block_pos == 0

		if not beg_aligned and end_block == beg_block:
			logger.debug("Write in the middle of a block")
			block = bytearray(self.read_block(beg_block))
			block[beg_block_pos:end_block_pos] = buf
			self.write_block(beg_block, block)
			return len(buf)
		elif not beg_aligned:
			logger.debug("Write in the end of a block")
			block = bytearray(self.read_block(beg_block))
			block[beg_block_pos:] = buf[:self.bs - beg_block_pos]
			self.write_block(beg_block, block)
			beg_block += 1

		# Write whole blocks in the middle
		for i in range(end_block-beg_block):
			idx_block = i + beg_block
			a = i * self.bs - beg_block_pos
			b = a + self.bs
			block = buf[a:b]
			logger.debug("Write block %d %d %d-%d", idx_block, len(block), a, b)
			self.write_block(idx_block, block)

		# Handle partial write at the end
		if end_block_pos is not None:
			logger.debug("Write end partial block %d ...%d", end_block, end_block_pos)
			block = bytearray(self.read_block(end_block))
			block[:end_block_pos] = buf[-end_block_pos:]
			self.write_block(end_block, block)

		#logger.debug("Wrote %d", len(buf))
		return len(buf)


	def read(self, fd, offset, size):
		logger.debug("Reading @%d, %d", offset, size)
		f = self.f

		if offset >= self.size:
			logger.debug("Zigouilling read")
			return b""
		elif offset + size > self.size:
			logger.debug("Truncating read, end %d > %d", offset + size, self.size)
			size = size - ((offset + size) - self.size)
			logger.debug("Truncated read to %d %d == %d", size, offset + size, self.size)
		else:
			logger.debug("zzzz %d to EOF", offset + size - self.size)

		if size == 0:
			return b""

		# Calculate the first and last blocks
		start_block = offset // self.bs
		end_block = (offset + size + self.bs-1) // self.bs
		logger.debug("Blocks [%d,%d[", start_block, end_block)

		# Calculate the position within the first and last block
		start_block_pos = offset % self.bs
		end_block_pos = (offset + size) % self.bs
		if end_block_pos == 0:
			end_block_pos = None

		decoded_blocks = []
		for block in range(start_block, end_block):
			try:
				u = self.read_block(block)
			except reedsolo.ReedSolomonError as e:
				logger.exception("Decode error: %s", e)
				if block == start_block:
					raise llfuse.FUSEError(errno.EIO)
				else:
					break

			if block == start_block:
				if start_block+1 == end_block:
					u = u[start_block_pos:end_block_pos]
				else:
					u = u[start_block_pos:]
			elif block == end_block-1:
				u = u[:end_block_pos]

			decoded_blocks.append(u)

		return b"".join(decoded_blocks)


def main(backing, virtual):

	attempts = 0
	while True:
		if os.path.exists(backing):
			break

		logger.debug("Waiting for backing file %s", backing)
		time.sleep(1)
		attempts += 1

	fs = Filesystem(backing)

	llfuse.init(fs, virtual, [])

	if "NOTIFY_SOCKET" in os.environ:
		import systemd.daemon
		res = systemd.daemon.notify("READY=1")
		if not res:
			logger.info("Couldn't notify of completed start-up")

	try:
		llfuse.main(workers=1)
	finally:
		llfuse.close()


if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(
	 description="FUSE filesystem",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument('backing',
	 help="",
	)

	parser.add_argument('virtual',
	 help="",
	)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	try:
		from exmakhina.logging.handler_lmdb import (
		 Handler as HandlerLMDB,
		)
		logging.root.addHandler(HandlerLMDB())
	except ModuleNotFoundError:
		pass

	main(args.backing, args.virtual)
