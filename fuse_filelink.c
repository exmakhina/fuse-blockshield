// SPDX-License-Identifier: GPL-3.0-only OR Proprietary
// SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-fuse_blockshield@zougloub.eu>
/*! \file
  \brief Toy FUSE filesystem performing identity-mapping of one backing file to a virtual file.

  Doesn't support file truncating.
*/
#define FUSE_USE_VERSION 31

#include <fuse3/fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>

//#define DEBUG_FILELINK

#if defined(DEBUG_FILELINK)
# define debug printf
#else
# define debug(...)
#endif

struct Filesystem {
	const char *source_path;
	int f;
};

static void* fs_init(struct fuse_conn_info */*conn*/, struct fuse_config */*cfg*/)
{
	return fuse_get_context()->private_data;
}

static int fs_getattr(const char */*path*/, struct stat *stbuf, struct fuse_file_info */*fi*/)
{
	struct Filesystem *fs = (struct Filesystem *)fuse_get_context()->private_data;
	debug("Getattr %s\n", fs->source_path);
	int res = stat(fs->source_path, stbuf);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int fs_open(const char */*path*/, struct fuse_file_info */*fi*/)
{
	debug("Open\n");
	return 0;
}


static int fs_read(const char */*path*/, char *buf, size_t size, off_t offset, struct fuse_file_info */*fi*/)
{
	struct Filesystem *fs = (struct Filesystem *)fuse_get_context()->private_data;
	debug("Read @ %jd / %zu\n", offset, size);
	return pread(fs->f, buf, size, offset);
}

static int fs_write(const char */*path*/, const char *buf, size_t size, off_t offset, struct fuse_file_info */*fi*/)
{
	struct Filesystem *fs = (struct Filesystem *)fuse_get_context()->private_data;
	debug("Write @ %jd / %zu\n", offset, size);
	return pwrite(fs->f, buf, size, offset);
}

static int fs_release(const char */*path*/, struct fuse_file_info */*fi*/)
{
	debug("Release\n");
	return 0;
}

static struct fuse_operations fs_ops = {
	.init = fs_init,
	.getattr = fs_getattr,
	.open = fs_open,
	.read = fs_read,
	.write = fs_write,
	.release = fs_release,
};

int main(int argc, char *argv[])
{
	int res;

	if (argc < 3) {
		fprintf(stderr, "Usage: %s <backing_file> <mount_point> [fuse options]\n", argv[0]);
		return 1;
	}

	struct Filesystem fs;
	fs.source_path = argv[1];
	fs.f = open(fs.source_path, O_RDWR | O_SYNC);

	struct fuse_args args = FUSE_ARGS_INIT(argc-2, &argv[2]);
	fuse_opt_parse(&args, NULL, NULL, NULL);

	struct fuse *fuse = fuse_new(&args, &fs_ops, sizeof(fs_ops), &fs);
	if (!fuse) {
		perror("fuse_new");
		return 1;
	}

	res = fuse_mount(fuse, argv[2]);
	if (res != 0) {
		perror("fuse_mount");
		return 1;
	}

	struct fuse_session * session = fuse_get_session(fuse);
	res = fuse_set_signal_handlers(session);

	int ret = fuse_loop(fuse);

	fuse_unmount(fuse);
	fuse_destroy(fuse);
	fuse_opt_free_args(&args);

	close(fs.f);

	return ret;
}

