# SPDX-License-Identifier: GPL-3.0-only OR Proprietary
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-fuse_blockshieldo@zougloub.eu>

from setuptools import setup

setup(
 name="fuse-blockshield",
 version="0.1",
 python_requires=">=3.7",
 description="FUSE filesystem",
 install_requires=[],
 packages=[
  "exmakhina.fuse_blockshield",
 ],
)
